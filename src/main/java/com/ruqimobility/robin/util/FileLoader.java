package com.ruqimobility.robin.util;

import com.alibaba.fastjson.JSON;
import lombok.SneakyThrows;

import java.io.InputStream;

/**
 * @author robin
 */
public class FileLoader {


    /**
     * 从类路径读取文件，并使用json反序列化为对象
     *
     * @param path  相对路径
     * @param clazz 类
     * @param <T>   类型
     * @return 对象
     */
    @SneakyThrows
    public static <T> T loadTestData(String path, Class<T> clazz) {
        InputStream testData = FileLoader.class.getClassLoader().getResourceAsStream(path);
        assert testData != null;
        return JSON.parseObject(testData, clazz);
    }
}
