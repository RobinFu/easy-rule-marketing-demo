package com.ruqimobility.robin.util;

import com.ruqimobility.robin.domain.MarketingCondition;
import com.ruqimobility.robin.domain.MarketingConditionPackage;
import com.ruqimobility.robin.domain.User;
import org.jeasy.rules.api.*;
import org.jeasy.rules.core.DefaultRulesEngine;

import java.util.List;

import static org.springframework.util.ObjectUtils.isEmpty;

/**
 * @author robin fu
 */
public class RuleHelper {

    /**
     * 对一个用户执行市场营销条件包
     *
     * @param rulePackage 市场营销条件包
     * @param user        指定用户
     */
    public static void fire(MarketingConditionPackage rulePackage, User user) {
        Rules rules = convert(rulePackage);
        //用easy rule engine对单个用户执行规则链条
        fire(user, rules);
    }

    /**
     * 用 easy rule engine对单个用户执行规则链条
     *
     * @param user  单个用户
     * @param rules 规则集
     */
    private static void fire(User user, Rules rules) {
        RulesEngine rulesEngine = new DefaultRulesEngine();
        Facts facts = new Facts();
        facts.put("user", user);
        rulesEngine.fire(rules, facts);
    }

    /**
     * 把市场营销条件包转换为easy-rule的规则集
     *
     * @param rulePackage 市场营销条件包
     * @return easy-rule的规则集
     */
    private static Rules convert(MarketingConditionPackage rulePackage) {
        List<MarketingCondition> conditions = rulePackage.getConditions();
        //注册到easy-rules
        Rules rules = new Rules();
        conditions.stream().map(RuleHelper::convert).forEach(rules::register);
        return rules;
    }

    /**
     * 把一条市场营销条件转为easy-rule
     *
     * @param marketingCondition 市场营销条件
     * @return easy-rule
     */
    public static Rule convert(MarketingCondition marketingCondition) {
        RobinRuleBuilder rule = new RobinRuleBuilder();
        rule = rule.name(marketingCondition.getName())
                .description(marketingCondition.getDescription())
                .priority(marketingCondition.getPriority())
                .when(marketingCondition.getCondition());

        //满足这个条件的话，发指定的优惠券
        if (!isEmpty(marketingCondition.getSendCoupons())) {
            rule.then("user.sendCoupons(\"" + String.join("\",\"", marketingCondition.getSendCoupons()) + "\")");
        }

        //链式执行子规则
        if (!isEmpty(marketingCondition.getSubConditions())) {
            Rules subRules = new Rules();
            marketingCondition.getSubConditions()
                    .stream()
                    .map(RuleHelper::convert)
                    .forEach(subRules::register);

            rule.then(facts -> {
                RulesEngine rulesEngine = new DefaultRulesEngine();
                rulesEngine.fire(subRules, facts);
            });
        }
        return rule.build();
    }
}
