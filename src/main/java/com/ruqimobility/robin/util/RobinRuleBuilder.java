package com.ruqimobility.robin.util;

import org.jeasy.rules.api.Action;
import org.jeasy.rules.api.Condition;
import org.jeasy.rules.core.RuleBuilder;
import org.jeasy.rules.mvel.MVELAction;
import org.jeasy.rules.mvel.MVELCondition;
import org.mvel2.ParserContext;


/**
 * 扩展原有的RuleBuilder
 * 加了when(java.lang.String)和then(java.lang.String)
 * 用来接收MVEL表达式的condition和action
 *
 * @author robin fu
 */
public class RobinRuleBuilder extends RuleBuilder {
    public RobinRuleBuilder() {
        super();
    }

    @Override
    public RobinRuleBuilder name(String name) {
        super.name(name);
        return this;
    }

    @Override
    public RobinRuleBuilder description(String description) {
        super.description(description);
        return this;
    }

    @Override
    public RobinRuleBuilder priority(int priority) {
        super.priority(priority);

        return this;
    }

    @Override
    public RobinRuleBuilder when(Condition condition) {
        super.when(condition);
        return this;
    }

    @Override
    public RobinRuleBuilder then(Action action) {
        super.then(action);
        return this;
    }

    /**
     * Specify the rule's condition as MVEL expression.
     *
     * @param condition of the rule
     * @return this RobinRuleBuilder
     */
    public RobinRuleBuilder when(String condition) {
        super.when(new MVELCondition(condition, new ParserContext()));
        return this;
    }

    /**
     * Add an action specified as an MVEL expression to the rule.
     *
     * @param action to add to the rule
     * @return this RobinRuleBuilder
     */
    public RobinRuleBuilder then(String action) {
        super.then(new MVELAction(action, new ParserContext()));
        return this;
    }
}
