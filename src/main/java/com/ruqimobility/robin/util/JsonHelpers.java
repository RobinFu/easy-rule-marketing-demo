package com.ruqimobility.robin.util;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * @Author jin.huang
 * @Date   2019年09月06 13:52
 **/
public final class JsonHelpers {

    public static String toJsonString(Object object, boolean prettyFormat) {
        return JSON.toJSONString(object, prettyFormat);
    }

    public static String toJsonString(Object object) {
        return JSON.toJSONString(object);
    }

    public static <T> T parseObject(String str, Class<T> clazz) {
        return JSON.parseObject(str, clazz);
    }

    public static <T> List<T> parseArray(String str, Class<T> clazz) {
        return JSON.parseArray(str, clazz);
    }
}
