package com.ruqimobility.robin.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 这个类是对用户的操作
 * 实际情况下，会在此类定义的各种方法中做一些复杂判断（比如查询数据库）来确定指定条件是否满足
 * 此demo中，只是简单模拟结果
 *
 * @author robin fu
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class User {

    private String userName;

    public boolean registerBetween(String dateStart, String dateEnd) {
        return dateStart.endsWith("01");
    }

    public boolean loginAfter(String date) {
        return date.endsWith("02");
    }

    public int noLoginDays() {
        return userName.hashCode() % 100;
    }

    public boolean noOrderInDays(int days) {
        return days % 2 == 0;
    }


    public int orderCount() {
        return userName.hashCode() % 33;
    }

    public boolean hasOrderDateAfter(String day) {
        return day.endsWith("3");
    }

    /**
     * 执行发优惠券动作，
     * 可以参考这个写法，做其他操作，比如命中风控条件，拉黑用户
     * @param coupons 要派发的优惠券列表
     */
    public void sendCoupons(String... coupons) {
        log.info("send coupons :" + String.join(",", coupons));
    }
}
