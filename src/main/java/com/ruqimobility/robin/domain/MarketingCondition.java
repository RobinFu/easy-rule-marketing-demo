package com.ruqimobility.robin.domain;

import lombok.Data;

import java.util.List;


/**
 * @author robin fu
 */
@Data
public class MarketingCondition {
    /**
     * 规则名（必填）
     */
    private String name;
    /**
     * 规则描述（可空）
     */
    private String description;
    /**
     * 优先级，数字越小越优先
     */
    private int priority;
    /**
     * 条件语句（必填）
     */
    private String condition;
    /**
     * 命中这个条件之后，发优惠券的列表（可空）
     */
    private List<String> sendCoupons;
    /**
     * 下一级条件
     */
    private List<MarketingCondition> subConditions;
}
