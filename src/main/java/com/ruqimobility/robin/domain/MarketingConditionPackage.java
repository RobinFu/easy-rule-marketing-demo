package com.ruqimobility.robin.domain;

import lombok.Data;

import java.util.List;

/**
 *
 * @author robin
 */
@Data
public class MarketingConditionPackage {
    /**
     * 名字
     */
    private String name;
    /**
     * 创建时间
     */
    private long createAt;
    /**
     * 描述
     */
    private String description;
    /**
     * 规则集
     */
    private List<MarketingCondition> conditions;

}
