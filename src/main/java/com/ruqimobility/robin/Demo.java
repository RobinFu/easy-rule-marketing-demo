package com.ruqimobility.robin;

import com.ruqimobility.robin.domain.MarketingConditionPackage;
import com.ruqimobility.robin.domain.User;
import com.ruqimobility.robin.util.FileLoader;
import com.ruqimobility.robin.util.RuleHelper;

/**
 * @author robin
 */
public class Demo {
    public static void main(String[] args) {
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "debug");

        MarketingConditionPackage conditionPackage =
                FileLoader.loadTestData("example-rules-1.json", MarketingConditionPackage.class);

        User u = new User("robinfu");
        RuleHelper.fire(conditionPackage, u);
    }
}
