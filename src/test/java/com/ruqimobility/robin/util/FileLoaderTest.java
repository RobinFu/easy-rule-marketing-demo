package com.ruqimobility.robin.util;

import org.junit.Test;

public class FileLoaderTest {

    @Test
    public void loadTestData() {
        String s = FileLoader.loadTestData("example-rules-1.json", String.class);
        System.out.println(s);
    }


    @Test(expected = AssertionError.class)
    public void loadTestData2() {
        String s = FileLoader.loadTestData("example-rules-2.json", String.class);
    }
}